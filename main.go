package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)


//Structs--------------------------------------
type apiData struct {
	uptime string `json:"uptime"`
	info string `json:"info"`
	version string `json:"version"`
}

type Track struct {
	Id 			 bson.ObjectId `bson:"_id,omitempty"`
	H_Date       time.Time `json:"Date"`
	Pilot        string    `json:"Pilot"`
	Glider       string    `json:"Glider Type"`
	Glider_id    string    `json:"Glider ID"`
	Track_length float64   `json:"TrackLength"`
	Track_src_url string 	`json:TrackUrl`
}

type trackDB struct {
	tracks map[int]Track
}

type Ticker struct {
	t_latest	int64 `json:"T_Latest"`
	t_start		int64 `json:"T_Start"`
	t_stop		int64 `json:"T_Stop"`
	Tracks[]	int	  `json:"ID"`
	processing	time.Duration `json:"processing"`

}
//---------------------------------------------
//Implementing MongoDB, i have no idea---------
type TrackStorage interface {
	Init()
	Add(t Track) error
	Get(id int) (Track, bool)
	GetField(field string, id int) (string, bool)
}
//---------------------------------------------

//Global variables-----------------------------
var startTime time.Time
var api = apiData{
	uptime: timeFormat(startTime),
	info:"Service for paragliding tracks.",
	version:"v1",
}
var db = trackDB{}
var IDs []int
var ticker = Ticker{}
//---------------------------------------------


//Ticker  funcs--------------------------------
func latestTrack (latestTime int64) {
	ticker.t_latest = latestTime/1000000
}
//---------------------------------------------



//ISO8601 format
func timeFormat (startTime time.Time) string{
	endTime := time.Now()
	return fmt.Sprintf("P%dY%dM%dDT%dH%dM%dS",
		endTime.Year()-startTime.Year(), endTime.Month()-startTime.Month(),
		endTime.Day()-startTime.Day(), endTime.Hour()-startTime.Hour(), endTime.Minute()-startTime.Minute(),
		endTime.Second()-startTime.Second())
}

/*func tickerFormat (latestTick time.Time) string{
	return fmt.Sprintf("P%dY%dM%dDT%dH%dM%dS",
		latestTick.Year(), latestTick.Month(), latestTick.Day(),
		latestTick.Hour(), latestTick.Minute(), latestTick.Second())

}*/

//GET funcs------------------------------------
func GetAPIinfo(w *http.ResponseWriter){
	api.uptime = timeFormat(startTime)
	json.NewEncoder(*w).Encode(api)
}

func GetTrackID (w *http.ResponseWriter){
	if len(db.tracks) == 0{
		json.NewEncoder(*w).Encode([]Track{})
	} else {
		json.NewEncoder(*w).Encode(IDs)
	}
}

func GetTrack (w *http.ResponseWriter, db *trackDB, ID string){
	IntID, _ := strconv.Atoi(ID)
	tra, status := db.Get(IntID)
	if !status {
		http.Error(*w, http.StatusText(404), http.StatusNotFound)
		return
	}else{
		json.NewEncoder(*w).Encode(tra)
	}

}

func GetTrackField (w *http.ResponseWriter, db *trackDB, ID string, field string){
	IntID, _ := strconv.Atoi(ID)
	tra, status := db.Get(IntID)
	if !status {
		http.Error(*w, http.StatusText(404), http.StatusNotFound)
	}
	if field == "Track_length"{
		fmt.Fprint(*w, tra.Track_length)
	} else if  field == "H_Date" {
		fmt.Fprint(*w, tra.H_Date)
	} else {
		returnField, ok := db.GetField(field, IntID)
		if ok {
			fmt.Printf(returnField)
		} else {
			http.Error(*w, http.StatusText(404), http.StatusNotFound)
			return
		}
	}


}

func GetTicker (w *http.ResponseWriter){
	var start = time.Now()
	//ticker.t_latest
	for i := 0; i<len(IDs) ;i++  {
		ticker.Tracks[i] = IDs[i]
	}

	ticker.processing = time.Since(start)
	json.NewEncoder(*w).Encode(ticker)
}

func GetLatest(w *http.ResponseWriter) {
	//Error
	fmt.Fprint(*w, ticker.t_latest)
}

//-----------------------------------------------

//Handler func-----------------------------------
func apiHandler(w http.ResponseWriter, r *http.Request){
	switch r.Method {
	case "POST":
		URL := strings.Split(r.URL.Path, "/")
		if len(URL) != 4{
			http.Error(w, http.StatusText(400), http.StatusBadRequest)
		}
		if URL[3] == "track" {
			if r.Body == nil {
				http.Error(w, "Wrong URL", http.StatusBadRequest)
				return
			}
			//Local variables
			url := make(map[string]string)
			var tra Track

			err := json.NewDecoder(r.Body).Decode(&url)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			track, err := igc.ParseLocation(url["url"])
			if err != nil {
				http.Error(w, "Cant read track", http.StatusBadRequest)
				return
			}

			tra.Pilot = track.Pilot
			tra.H_Date = track.Header.Date
			tra.Track_length = track.Points[0].Distance(track.Points[len(track.Points)-1])
			tra.Glider = track.GliderType
			tra.Glider_id = track.GliderID
			//TODO, add url to track.Track_src_url
			var tick = time.Now().UnixNano()
			latestTrack(tick)

			id := db.Add(tra)
			if len(IDs) == 0 {
				ticker.t_start = tick/1000000
			}
			IDs = append(IDs, id)
			json.NewEncoder(w).Encode(id)
			return

		} else {
			http.Error(w, http.StatusText(404), http.StatusBadRequest)
			return
		}

	case "GET":
		URL := strings.Split(r.URL.Path, "/")

		if len(URL) == 6 || URL[5] == "latest"{
			http.Header.Add(w.Header(), "content-type", "text/plain")
		} else {
			http.Header.Add(w.Header(), "content-type", "application/json")
		}

		if URL[1] == "paragliding" && URL[2] != "api" {
			http.Redirect(w, r, "api", http.StatusSeeOther)
			return
		} else if len(URL) == 5 && URL[3] != "igc" || len(URL) == 6 && URL[3] != "igc" {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		}

		if URL[2] == "api" && len(URL) == 3 {
			GetAPIinfo(&w) 		//Handel /api
		} else if URL[3] == "igc" && len(URL) == 4 {
			GetTrackID(&w)			//Handel /api/igc
		} else if URL[4] != "" && len(URL) == 5 {
			GetTrack(&w, &db, URL[4]) //Handel /api/igc/<id>
		} else if URL[5] != "" && len(URL) == 6 {
			GetTrackField(&w, &db, URL[4], URL[5])
		} else if URL[3] == "ticker" && len(URL) == 4 {
			GetTicker(&w)
		} else if URL[4] == "latest" && len(URL) == 5{
			GetLatest(&w)
		} else {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		}
	}

}

func adminHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	switch r.Method {
	case "GET":
		if len(db.tracks) == 0 {
			http.Error(w, "No tracks in DB", http.StatusBadRequest)
		} else {
			fmt.Fprint(w, len(db.tracks))
		}

	case "DELETE":
		for k := range db.tracks {
			delete(db.tracks, k)
		}

	default:
		http.Error(w, "Not yet implemented", http.StatusNotImplemented)
		return

	}
}
//-----------------------------------------------

//Main
func main() {

	Global_db = &IgcMongoDB{"mongodb://olebgr:password1@ds145053.mlab.com:45053/igcinfo",
	"igcinfo",
	"tracks"}

	db.Init()			//Initialising localy


	Global_db.Init()	//Initialising MongoDB

	startTime = time.Now()

	var p string
	if port := os.Getenv("PORT"); port != "" {
		p = ":" + port
	} else {
		p = ":8080"
	}
	http.HandleFunc("/paragliding/", apiHandler)
	http.HandleFunc("/paragliding/admin/", adminHandler)
	http.ListenAndServe(p, nil)

}

