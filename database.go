package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"reflect"
)


var Global_db TrackStorage


type IgcMongoDB struct {
	DatabaseURL string
	DatabaseName string
	TracksCollectionName string
}


func (db *IgcMongoDB) Init() {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil{
		panic(err)
	}
	defer session.Close()

	index := mgo.Index{
		Key: []string{},
		Unique: true,
		DropDups: true,
		Background: true,
		Sparse: true,
	}

	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).EnsureIndex(index)
	if err != nil {
		panic(err)
	}
}

func (db *IgcMongoDB) Get(id int) (Track, bool) {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil{
		panic(err)
	}
	defer session.Close()

	track := Track{}
	allWasGood := true

	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Find(IDs[id]).One(&track)
	if err != nil {
		allWasGood = false
	}

	return track, allWasGood
}

func (db *IgcMongoDB) Add(t Track) error {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil{
		panic(err)
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Insert(t)
	if err != nil {
		fmt.Printf("Error in Insert(): %v", err.Error())
		return err
	}

	return nil
}

func (db *IgcMongoDB) GetField(field string, id int) (string, bool) {
	session, err := mgo.Dial(db.DatabaseName)
	if err != nil{
		panic(err)
	}
	defer session.Close()

	isField := true
	track := Track{}

	err = session.DB(db.DatabaseName).C(db.TracksCollectionName).Find(IDs[id]).One(&track)
	if err != nil {
		panic(err)
	}
	val := reflect.ValueOf(track).FieldByName(field)
	if val.String() == "<Invalid Value>" {
		isField = false
	}
	return string(val.String()), isField

}