package main

import (
	"math/rand"
	"reflect"
)

//Track  funcs---------------------------------
func (db *trackDB) Init() {
	db.tracks = make(map[int]Track)
}

func (db *trackDB) Get(id int) (Track, bool) {
	t, ok := db.tracks[id]
	return t, ok
}

func (db *trackDB) GetField(field string, id int) (string, bool){
	isField := true
	val := reflect.ValueOf(db.tracks[id])
	fi := reflect.Indirect(val).FieldByName(field)
	if fi.String() == "<invalid Value>"{
		isField = false
	}
	return string(fi.String()),isField
}

func (db *trackDB) Add(t Track) int {
	//New ID
	id := rand.Int()
	_, ok := db.Get(id)

	//If exists, create new ID
	for ok {
		id = rand.Int()
		_, ok = db.Get(id)
	}

	db.tracks[id] = t

	return id
}
//---------------------------------------------
